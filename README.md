# 加载我们的库
import pandas as pd
import numpy as np
# 使用 re 写正则匹配
import re
# 使用 sklearn 的模型来进行预测
import sklearn
# 使用 seaborn 进行可视化
import seaborn as sns
import matplotlib.pyplot as plt
%matplotlib inline

# 加载 train 和 test 数据集
train = pd.read_csv('../input/train.csv')
test = pd.read_csv('../input/test.csv')

# 存储 Passenger ID 方便后续操作
PassengerId = test['PassengerId']

train.head(3)

# 整体的全部数据
full_data = [train, test]

# 一些从原始 features 中衍生出的 features ，我认为可以算是一个 feature
# Name_length 特征
train['Name_length'] = train['Name'].apply(len)
test['Name_length'] = test['Name'].apply(len)
# 在 Titanic 上是否具有一个 cabin
train['Has_Cabin'] = train["Cabin"].apply(lambda x: 0 if type(x) == float else 1)
test['Has_Cabin'] = test["Cabin"].apply(lambda x: 0 if type(x) == float else 1)

# 创建一个新的 feature FamilySize 作为 SibSp 和 Parch 的混合 features
for dataset in full_data:
    dataset['FamilySize'] = dataset['SibSp'] + dataset['Parch'] + 1
# 从 FamilySize 特征中 创建一个新的 feature IsAlone
for dataset in full_data:
    dataset['IsAlone'] = 0
    dataset.loc[dataset['FamilySize'] == 1, 'IsAlone'] = 1
# 删除 Embarked 列中的所有的 NULLS值，使用 S 来填充（算是超级暴力的吧）
for dataset in full_data:
    dataset['Embarked'] = dataset['Embarked'].fillna('S')
# 删除 Fare 中所有的 NULLS 值，并生成一个新的 feature 列 CategoricalFare
for dataset in full_data:
    dataset['Fare'] = dataset['Fare'].fillna(train['Fare'].median())
train['CategoricalFare'] = pd.qcut(train['Fare'], 4)
# 创建一个新特征 CategoricalAge
for dataset in full_data:
    age_avg = dataset['Age'].mean()
    age_std = dataset['Age'].std()
    age_null_count = dataset['Age'].isnull().sum()
    age_null_random_list = np.random.randint(age_avg - age_std, age_avg + age_std, size=age_null_count)
    dataset['Age'][np.isnan(dataset['Age'])] = age_null_random_list
    dataset['Age'] = dataset['Age'].astype(int)
train['CategoricalAge'] = pd.cut(train['Age'], 5)


# 定义函数从 passenger 名字中获取 titles
def get_title(name):
    title_search = re.search(' ([A-Za-z]+)\.', name)
    # 如果 title 存在，返回
    if title_search:
        return title_search.group(1)
    return ""


# 创建一个新的特征 Title, 包含乘客的名字的 titles
for dataset in full_data:
    dataset['Title'] = dataset['Name'].apply(get_title)
# 将所有的非常见的 title 分类到一个 “Rare” 特征
for dataset in full_data:
    dataset['Title'] = dataset['Title'].replace(
        ['Lady', 'Countess', 'Capt', 'Col', 'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')

    dataset['Title'] = dataset['Title'].replace('Mlle', 'Miss')
    dataset['Title'] = dataset['Title'].replace('Ms', 'Miss')
    dataset['Title'] = dataset['Title'].replace('Mme', 'Mrs')

# 将数据集中的数据映射成离散型数据
for dataset in full_data:
    # 映射 Sex
    dataset['Sex'] = dataset['Sex'].map({'female': 0, 'male': 1}).astype(int)

    # 映射 titles
    title_mapping = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}
    dataset['Title'] = dataset['Title'].map(title_mapping)
    dataset['Title'] = dataset['Title'].fillna(0)

    # 映射 Embarked
    dataset['Embarked'] = dataset['Embarked'].map({'S': 0, 'C': 1, 'Q': 2}).astype(int)

    # 映射 Fare
    dataset.loc[dataset['Fare'] <= 7.91, 'Fare'] = 0
    dataset.loc[(dataset['Fare'] > 7.91) & (dataset['Fare'] <= 14.454), 'Fare'] = 1
    dataset.loc[(dataset['Fare'] > 14.454) & (dataset['Fare'] <= 31), 'Fare'] = 2
    dataset.loc[dataset['Fare'] > 31, 'Fare'] = 3
    dataset['Fare'] = dataset['Fare'].astype(int)

    # 映射 Age
    dataset.loc[dataset['Age'] <= 16, 'Age'] = 0
    dataset.loc[(dataset['Age'] > 16) & (dataset['Age'] <= 32), 'Age'] = 1
    dataset.loc[(dataset['Age'] > 32) & (dataset['Age'] <= 48), 'Age'] = 2
    dataset.loc[(dataset['Age'] > 48) & (dataset['Age'] <= 64), 'Age'] = 3
    dataset.loc[dataset['Age'] > 64, 'Age'] = 4

    # Feature selection
    drop_elements = ['PassengerId', 'Name', 'Ticket', 'Cabin', 'SibSp']
    train = train.drop(drop_elements, axis=1)
    train = train.drop(['CategoricalAge', 'CategoricalFare'], axis=1)
    test = test.drop(drop_elements, axis=1)


    # 生成一个临时文件，专门存储我们已经转化/映射完成之后的 train 和 test 文件，
    def saveTmpTrainFile(tmpFile, csvName):
        with open(csvName, 'w', newline='') as myFile:
            myWriter = csv.writer(myFile)
            myWriter.writerow(
                ["Survived", "Pclass", "Sex", "Age", "Parch", "Fare", "Embarked", "Name_length", "Has_cabin",
                 "FamilySize", "IsAlone", "Title"])
            for lines in tmpFile.index:
                tmp = []
                tmp.append(tmpFile.loc[lines].values[1])
                tmp.append(tmpFile.loc[lines].values[2])
                tmp.append(tmpFile.loc[lines].values[4])
                tmp.append(tmpFile.loc[lines].values[5])
                tmp.append(tmpFile.loc[lines].values[7])
                tmp.append(tmpFile.loc[lines].values[9])
                tmp.append(tmpFile.loc[lines].values[11])
                tmp.append(tmpFile.loc[lines].values[12])
                tmp.append(tmpFile.loc[lines].values[13])
                tmp.append(tmpFile.loc[lines].values[14])
                tmp.append(tmpFile.loc[lines].values[15])
                tmp.append(tmpFile.loc[lines].values[-1])
                myWriter.writerow(tmp)


    saveTmpTrainFile(train, 'D:/titanic/titanic_dataset/train_later.csv')


    # 生成一个临时文件，专门存储我们已经转化/映射完成之后的 train 和 test 文件
    def saveTmpTestFile(tmpFile, csvName):
        with open(csvName, 'w', newline='') as myFile:
            myWriter = csv.writer(myFile)
            myWriter.writerow(
                ["Pclass", "Sex", "Age", "Parch", "Fare", "Embarked", "Name_length", "Has_cabin", "FamilySize",
                 "IsAlone", "Title"])
            for lines in tmpFile.index:
                tmp = []
                tmp.append(tmpFile.loc[lines].values[1])
                tmp.append(tmpFile.loc[lines].values[3])
                tmp.append(tmpFile.loc[lines].values[4])
                tmp.append(tmpFile.loc[lines].values[6])
                tmp.append(tmpFile.loc[lines].values[8])
                tmp.append(tmpFile.loc[lines].values[10])
                tmp.append(tmpFile.loc[lines].values[11])
                tmp.append(tmpFile.loc[lines].values[12])
                tmp.append(tmpFile.loc[lines].values[13])
                tmp.append(tmpFile.loc[lines].values[14])
                tmp.append(tmpFile.loc[lines].values[15])
                myWriter.writerow(tmp)


    # 存储 test 文件
    saveTmpFile(test, 'D:/titanic/titanic_dataset/test_later.csv')
